# Dummy Content Signer

These modules will be content-agnostic, meaning they are themselves responsible for collecting the content they need to sign. The content will be collected by an internal Business Logic which is out of scope for OCM W-Stack.Since implementing an actual singer could be complicated depending on the algorithms used and content required, we will create a dummy signer which will generate dummy content and add a "signature".Example Content:
```
{
    "id": "ocm-id-5",
    "type": "dummyCredential"
    "issuer": "https:ocmw-host.dev"
    "credentialSubject": {
        "name": "Hans Peter",
        "age": 74
    }
}
```
The signer would add a proof like this:
```
{
    "id": "ocm-id-5",
    "type": "dummyCredential"
    "issuer": "https:ocmw-host.dev"
    "credentialSubject": {
        "name": "Hans Peter",
        "age": 74
    },
    "proof": {
        "type": "dummy",
        "created": "<Date.now()>"
        "verificationMethod": "None",
        "proofPurpose": "development",
        "proofValue": "dummyProof"
        "tenant": "<tenantid>"
    }
}
```

### Inputs to the signer Modules:
All Modules should be using a unified interface. The Event we define should contain the following input:
- tenantid
- requested proof algorithm