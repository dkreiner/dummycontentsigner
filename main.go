package main

import (
	"context"
	"dummycontentsigner/api"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"log"
)

type dummyConfig struct {
	config.BaseConfig `mapstructure:",squash"`
	cloudeventprovider.Config
	DummyTopic string `mapstructure:"creationTopic"`
}

var conf dummyConfig

func main() {
	ctx := context.Background()

	if err := config.LoadConfig("DUMMYCONTENTSIGNER", &conf, getDefaults()); err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	logger, err := logr.New(conf.LogLevel, conf.IsDev, nil)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	ctx = ctxPkg.WithLogger(ctx, *logger)

	api.Listen(ctx, conf.DummyTopic)
}

func getDefaults() map[string]any {
	return map[string]any{
		"isDev":      false,
		"dummyTopic": "dummy",
	}
}
