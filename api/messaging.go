package api

import (
	"context"
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	"sync"
)

func startMessaging(ctx context.Context, topic string, group *sync.WaitGroup) {
	defer group.Done()

	log = ctxPkg.GetLogger(ctx)

	log.Info("start messaging!")

	client, err := cloudeventprovider.NewClient(cloudeventprovider.ConnectionTypeSub, topic)
	if err != nil {
		log.Error(err, "")
	}

	err = client.ReplyCtx(ctx, handleStuff)
	if err != nil {
		log.Error(err, "")
	}
}

func handleStuff(ctx context.Context, event event.Event) (*event.Event, error) {

}
