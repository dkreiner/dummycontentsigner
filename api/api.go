package api

import (
	"context"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"sync"
)

var log logr.Logger

func Listen(ctx context.Context, dummyTopic string) {
	var wg sync.WaitGroup

	wg.Add(1)
	go startMessaging(ctx, dummyTopic, &wg)
	wg.Wait()
}
